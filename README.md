# tsWillDoc
此為 保哥 ts 翻譯文章練習專案
https://willh.gitbook.io/typescript-tutorial


## get start
1. ts 環境安裝
> npm i -g typescript

=> 本專案於根目錄執行即可, 需要編譯ts 指令路徑就好

2. ts 編譯
> tsc hello.ts

=> 本專案需指定路徑, 如:
> tsc 3-advanced/7-generics/index.ts


參考文章:
https://willh.gitbook.io/typescript-tutorial/introduction/get-typescript


## 版本
v1.1
1. 新增基本 readme.md   2024/03/19


